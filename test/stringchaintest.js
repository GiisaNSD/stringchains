const StringChainContract = artifacts.require("StringChain");
const util = require('util');

contract('StringChain2', (accounts) => {
  
  let deployContract;
  
  beforeEach(async () => {
        deployContract = await StringChainContract.deployed();
  });

  it('Should accept a normal string', async () => {
    const tx = await deployContract.register("Testing");

    const { logs } = tx;
    
    const log = logs[0];
    console.log(log);
    assert(log.args.data === "Testing");
    assert(logs.length == 1);
    assert(log.event === "Registered");

  });

  it('Should accept long string', async () => {
    const tx = await deployContract.register("Lorem ipsum dolor sit amet consectetur adipiscing elit nostra facilisis nascetur litora, sapien justo quam praesent aptent conubia platea lobortis nulla risus felis, curae proin sodales congue tortor netus mollis ultricies sem donec. Ut a facilisi lectus etiam vitae tempor sociis proin curae, varius ridiculus potenti habitant aenean sapien eleifend tincidunt. Malesuada rutrum nec placerat dui accumsan facilisis nascetur, per ac taciti ornare volutpat nulla sodales vestibulum, interdum phasellus platea vulputate eget dictum. Lectus condimentum faucibus at senectus donec mattis viverra lobortis fusce, erat tristique himenaeos natoque facilisi dignissim inceptos mauris nisl dis, ante sed accumsan integer felis ad tortor sem. Aptent at tincidunt vivamus pharetra senectus aenean, etiam purus ut suspendisse a tempus, quam ultrices venenatis arcu morbi. Placerat pellentesque massa molestie dui duis sapien varius facilisi tempus lacinia, mauris rutrum vivamus fermentum volutpat natoque dictumst porttitor ullamcorper, suscipit parturient mus quisque pretium non eget urna morbi.");

    const { logs } = tx;
    
    const log = logs[0];
    console.log(log);
    assert(log.args.data === "Lorem ipsum dolor sit amet consectetur adipiscing elit nostra facilisis nascetur litora, sapien justo quam praesent aptent conubia platea lobortis nulla risus felis, curae proin sodales congue tortor netus mollis ultricies sem donec. Ut a facilisi lectus etiam vitae tempor sociis proin curae, varius ridiculus potenti habitant aenean sapien eleifend tincidunt. Malesuada rutrum nec placerat dui accumsan facilisis nascetur, per ac taciti ornare volutpat nulla sodales vestibulum, interdum phasellus platea vulputate eget dictum. Lectus condimentum faucibus at senectus donec mattis viverra lobortis fusce, erat tristique himenaeos natoque facilisi dignissim inceptos mauris nisl dis, ante sed accumsan integer felis ad tortor sem. Aptent at tincidunt vivamus pharetra senectus aenean, etiam purus ut suspendisse a tempus, quam ultrices venenatis arcu morbi. Placerat pellentesque massa molestie dui duis sapien varius facilisi tempus lacinia, mauris rutrum vivamus fermentum volutpat natoque dictumst porttitor ullamcorper, suscipit parturient mus quisque pretium non eget urna morbi.");
    assert(logs.length == 1);
    assert(log.event === "Registered");

  });

  it('Shouldnt accept numbers', async () => {
    const tx = await deployContract.register(123456879);

    const { logs } = tx;
    
    const log = logs[0];

    console.log(log);

    assert(log.args.data === '');
    assert(logs.length == 1);
    assert(log.event === "Registered");
  });

  it('Should accept three strings concatenated whit +', async () => {
    const tx = await deployContract.register("Testing" + "This" + "String");

    const { logs } = tx;
    
    const log = logs[0];
    console.log(log);
    assert(log.args.data === 'TestingThisString');
    assert(logs.length == 1);
    assert(log.event === "Registered");

  });

  it('Should accept code as string', async () => {
    const tx = await deployContract.register("<svg width='320' height='352' viewBox='0 0 320 352' fill='none' xmlns='http://www.w3.org/2000/svg'><rect x='5' y='5' width='310' height='342' fill='#FAF6F6' stroke='black' stroke-width='10'/><ellipse cx='160' cy='210' rx='64' ry='60' fill='#171515'/><ellipse cx='96.5' cy='150.5' rx='47.5' ry='42.5' fill='#171515'/><ellipse cx='227.5' cy='150.5' rx='47.5' ry='42.5' fill='#171515'/></svg>");

    const { logs } = tx;
    
    const log = logs[0];
    console.log(log);
    assert(log.args.data === "<svg width='320' height='352' viewBox='0 0 320 352' fill='none' xmlns='http://www.w3.org/2000/svg'><rect x='5' y='5' width='310' height='342' fill='#FAF6F6' stroke='black' stroke-width='10'/><ellipse cx='160' cy='210' rx='64' ry='60' fill='#171515'/><ellipse cx='96.5' cy='150.5' rx='47.5' ry='42.5' fill='#171515'/><ellipse cx='227.5' cy='150.5' rx='47.5' ry='42.5' fill='#171515'/></svg>");
    assert(logs.length == 1);
    assert(log.event === "Registered");
  });

  it('Should accept binary code as string', async () => {
    const tx = await deployContract.register("01010011 01101001 00100000 01110100 01110010 01100001 01100100 01110101 01100011 01101001 01110011 00100000 01100101 01110011 01110100 01100101 00100000 01100011 01101111 01100100 01101001 01100111 01101111 00101100 00100000 01110100 01100101 00100000 01100100 01100101 01110011 01100101 01101111 00100000 01110101 01101110 00100000 01101101 01110101 01111001 00100000 01100010 01110101 01100101 01101110 00100000 01100100 01101001 01100001");

    const { logs } = tx;
    
    const log = logs[0];
    console.log(log);
    assert(log.args.data === '01010011 01101001 00100000 01110100 01110010 01100001 01100100 01110101 01100011 01101001 01110011 00100000 01100101 01110011 01110100 01100101 00100000 01100011 01101111 01100100 01101001 01100111 01101111 00101100 00100000 01110100 01100101 00100000 01100100 01100101 01110011 01100101 01101111 00100000 01110101 01101110 00100000 01101101 01110101 01111001 00100000 01100010 01110101 01100101 01101110 00100000 01100100 01101001 01100001');
    assert(logs.length == 1);
    assert(log.event === "Registered");

  });

  it('Should accept special characters', async () => {
    const tx = await deployContract.register("_as{ñ}.;,[¨POASD?!=))12##°¬~`^AaB12Uu|");

    const { logs } = tx;
    
    const log = logs[0];
    console.log(log);
    assert(log.args.data === '_as{ñ}.;,[¨POASD?!=))12##°¬~`^AaB12Uu|');
    assert(logs.length == 1);
    assert(log.event === "Registered");

  });

});

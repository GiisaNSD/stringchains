const ConvertLib = artifacts.require("ConvertLib");
const StringChain = artifacts.require("StringChain");

module.exports = function(deployer) {
  deployer.deploy(ConvertLib);
  deployer.link(ConvertLib, StringChain);
  deployer.deploy(StringChain);
};

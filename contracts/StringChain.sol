//SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

contract StringChain {

    event Registered(string data);

    function register(string memory data) public {

        emit Registered(data);  

    }

}